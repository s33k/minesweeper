from enum import Enum

import pygame


class Field:

    class State(Enum):
        NORMAL = 1
        ACTIVE = 2
        BOMB = 3
        DANGER = 4


    count = 0
    bomb = False
    field_normal = (0, 135, 255)
    field_active = (137, 200, 255)
    field_danger = (255, 157, 137)
    msg = ''

    state = State.NORMAL

    def __init__(self, x, y, w, h, screen):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.screen = screen
        self.font = pygame.font.SysFont("arial", 20)

    def frame(self, event):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        if self.x + self.w > mouse[0] > self.x and self.y + self.h > mouse[1] > self.y:
            if click[0] == 1 and event.type == pygame.MOUSEBUTTONDOWN:
                return self.action()
            elif click[2] == 1 and event.type == pygame.MOUSEBUTTONDOWN:
                self.action_right()
                return 1

        pygame.draw.rect(self.screen, self.get_image(), (self.x, self.y, self.w, self.h))

        if self.state == self.State.ACTIVE:
            if self.count > 0:
                self.msg = str(self.count)
            if self.bomb:
                self.msg = 'X'


        text = self.font.render(self.msg, False, (255, 255, 255))
        self.screen.blit(text, (self.x + text.get_width(), self.y + text.get_height() / 2))
        return 1
    def action(self):
        if not self.state == self.State.DANGER:
            if not self.bomb and self.count == 0:
                self.state = self.State.ACTIVE
                return 0
            if self.count > 0:
                self.state = self.State.ACTIVE
                return 1
            if self.bomb:
                self.state = self.State.BOMB
                return 2

    def get_image(self):
        return {
            self.State.ACTIVE: self.field_active,
            self.State.NORMAL: self.field_normal,
            self.State.BOMB: self.field_danger,
            self.State.DANGER: self.field_danger,
        }[self.state]

    def action_right(self):
        if self.state == self.State.NORMAL:
            self.state = self.State.DANGER
        elif self.state == self.State.DANGER:
            self.state = self.State.NORMAL
        pass

