import field as field
import pygame
from field import Field
from random import randint

w, h = 10, 10
array = [[0 for x in range(w)] for y in range(h)]
fields_checked = []


def activate_mass(xh, xw):
    field = array[xh][xw]
    for i in range(xh - 1, xh + 2):
        for j in range(xw - 1, xw + 2):
            if i == xh and j == xw:
                continue
            if i < 0 or i == h or j < 0 or j == w:
                continue
            else:
                if array[i][j].action() == 0:
                    fields_checked.append((i, j))


def free_fields(xh, xw):
    fields_checked.append((xh, xw))
    for i in fields_checked:
        activate_mass(i[0], i[1])
    fields_checked.clear()


def fields_frame(event):
    for i in range(h):
        for j in range(w):
            if array[i][j].frame(event) == 0:
                free_fields(i, j)


def bomb_draw():
    for r in range(10):
        array[randint(0, h - 1)][randint(0, w - 1)].bomb = True


def set_count(xh, xw):
    field = array[xh][xw]
    for i in range(xh - 1, xh + 2):
        for j in range(xw - 1, xw + 2):
            if i == xh and j == xw:
                continue
            if i < 0 or i == h or j < 0 or j == w:
                continue
            else:
                if array[i][j].bomb:
                    field.count += 1


def set_counts():
    for i in range(h):
        for j in range(w):
            if array[i][j].bomb:
                continue
            else:
                set_count(i, j)


pygame.init()
screen = pygame.display.set_mode((700, 700))
done = False
is_blue = True

for i in range(h):
    for j in range(w):
        array[i][j] = Field(j * 50 + 100, i * 50 + 100, 40, 40, screen)
bomb_draw()
set_counts()

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            is_blue = not is_blue

        fields_frame(event)

        color = ()
        if is_blue:
            color = (0, 128, 255)
        else:
            color = (255, 100, 0)

        # pygame.draw.rect(screen, color, pygame.Rect(30, 30, 60, 60))
        pygame.display.flip()
